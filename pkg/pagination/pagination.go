package pagination

// Paginator - Paginator.
type Paginator struct {
	Page  int
	Limit int
	Count int
}

// DefaultRange - Look at Paginator.Range().
var DefaultRange = 2

// New - New pagination.
// Page cannot be zero or less. It's 1 by default.
// Limit cannot be zero or less. It's 10 by default.
// Count is number of elements to paginate.
func New(page, limit, count int) *Paginator {
	// Default page is 1
	if page <= 0 {
		page = 1
	}

	// Default limit is 10
	if limit <= 0 {
		limit = 10
	}

	return &Paginator{
		Page:  page,
		Limit: limit,
		Count: count,
	}
}

// NextPage - Returns next page.
// Returns zero if there is no next page.
func (p *Paginator) NextPage() int {
	// If current page is last page
	if p.Page >= p.LastPage() {
		return 0
	}
	return p.Page + 1
}

// PrevPage - Returns previous page.
// Returns zero if current page is first (or less).
func (p *Paginator) PrevPage() int {
	// If its first page there is no previous
	if p.Page <= 1 {
		return 0
	}
	return p.Page - 1
}

// Range - Returns array of ints containing numbers in range
// (currentPage - DefaultRange) to (currentPage + DefaultRange).
// End page can be max LastPage.
func (p *Paginator) Range() []int {
	// Start page (minimum 1)
	startPage := p.Page - DefaultRange
	if startPage <= 0 {
		startPage = 1
	}

	// End page (maximum - LastPage)
	lastPage := p.Page + DefaultRange
	if lastPage > p.LastPage() {
		lastPage = p.LastPage()
	}

	// Make list
	length := lastPage - startPage + 1
	list := make([]int, length)

	// Add pages
	for i := 0; i < length; i++ {
		list[i] = startPage + i
	}

	return list
}

// Offset - Offset by page.
// If page is less than zero returns 0.
func (p *Paginator) Offset() int {
	if p.Page <= 1 {
		return 0
	}
	return (p.Page - 1) * p.Limit
}

// FirstPage - Returns one.
func (p *Paginator) FirstPage() int {
	return 1
}

// LastPage - Returns last page.
func (p *Paginator) LastPage() int {
	if p.Count == 0 {
		return 0
	}
	return (p.Count / p.Limit) + 1
}
