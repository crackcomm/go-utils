// Package hash implements helpers for computing hashes with salt.
package hash

import (
	"bytes"
	"crypto/sha1"
	"crypto/sha256"
	"fmt"
	"strings"
)

// New - Creates new sha1 hash with salt.
// Hash is created from salt:value.
// Returns hex-encoded hash.
func New(salt, value string) string {
	value = strings.Join([]string{salt, value}, ":")
	hash := sha1.Sum([]byte(value))
	return fmt.Sprintf("%x", hash)
}

// NewSha256 - Creates new sha256 hash.
// Hash is created from salt:value.
// Returns hex-encoded hash.
func NewSha256(salt, value string) string {
	value = strings.Join([]string{salt, value}, ":")
	hash := sha256.Sum256([]byte(value))
	return fmt.Sprintf("%x", hash)
}

// NewSha256Bytes - Creates new sha256 hash.
// Hash is created from salt:value.
// Returns hex-encoded hash.
func NewSha256Bytes(salt, value []byte) (h []byte) {
	value = bytes.Join([][]byte{salt, value}, []byte(":"))
	return sha256.New().Sum(value)
}
