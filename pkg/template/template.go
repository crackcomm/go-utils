// Package template implements helpers for pongo2.
package template

import (
	"bytes"
	"flag"
	"io"
	"net/http"

	pongo "github.com/flosch/pongo2"
	log "github.com/golang/glog"
)

// Context - Template context.
type Context pongo.Context

// Defaults - Context defaults.
var Defaults = Context{}

// templates - Template set.
var templates = pongo.NewSet("templates")

// templateDir - Directory containing all templates.
var templateDir = "templates"

// InitFlags - Initializes flags in default FlagSet.
func InitFlags() {
	flag.StringVar(&templateDir, "templates-dir", templateDir, "Templates directory")
	flag.BoolVar(&templates.Debug, "templates-dev", false, "Development mode")
}

// Init - Initializes templates.
func Init() error {
	// Parse flags
	if !flag.Parsed() {
		flag.Parse()
	}

	// Set templates base directories
	return templates.SetBaseDirectory(templateDir)
}

// Render - Renders template to http response.
// If error occurs logs and writes to response.
func Render(w http.ResponseWriter, r *http.Request, name string, ctx Context) {
	// Create empty context if nil
	if ctx == nil {
		ctx = Context{}
	}

	// Set context HTTP Request
	ctx["request"] = r

	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	err := RenderWriter(w, name, ctx)
	if err != nil {
		log.Warningf("Error rendering template %q on %s %s: %v", name, r.Method, r.RequestURI, err)
		http.Error(w, "Template error", http.StatusInternalServerError)
	}
}

// RenderWriter - Renders template to writer.
func RenderWriter(w io.Writer, name string, ctx Context) (err error) {
	// Parse template
	t, err := templates.FromCache(name)
	if err != nil {
		return
	}

	// Set context defaults if any
	ctx = ctxWithDefaults(ctx)

	// Render template
	return t.ExecuteWriter(pongo.Context(ctx), w)
}

// RenderBytes - Renders template to byte array.
func RenderBytes(name string, ctx Context) ([]byte, error) {
	buffer := new(bytes.Buffer)
	if err := RenderWriter(buffer, name, ctx); err != nil {
		return nil, err
	}
	return buffer.Bytes(), nil
}

// SetDefault - Sets template context default.
func SetDefault(key string, value interface{}) {
	Defaults[key] = value
}

// Merge - Merges keys and values from first argument into current context.
func (c Context) Merge(ctx Context) {
	for key, value := range ctx {
		c[key] = value
	}
}

func ctxWithDefaults(ctx Context) Context {
	if ctx == nil {
		return Defaults
	}

	if len(Defaults) <= 0 {
		return ctx
	}

	for key, value := range Defaults {
		if _, ok := ctx[key]; !ok {
			ctx[key] = value
		}
	}
	return ctx
}
